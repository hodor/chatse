'use strict';

/**
 * @ngdoc overview
 * @name chatseApp
 * @description
 * # chatseApp
 *
 * Main module of the application.
 */
angular
  .module('chatseApp', [
    'ngRoute',
    'ngMaterial'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      });
  })
  .config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey')
    .accentPalette('lime');
  })
  .service('chatService', ['$q',
    function ($q) {
      var chats = [{
        name: 'Gandalf',
        content: ''
    }, {
        name: 'Aragorn',
        content: ''
    }, {
        name: 'Frodo',
        content: ''
    }, {
        name: 'Sauron',
        groupName: 'Mordor',
        members: [],
        content: ''
    }];

      return {
        loadAll: function () {
          return $q.when(chats);
        }
      };
  }]);
